module github.com/jmmal/runs-api

go 1.14

require (
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/google/go-cmp v0.5.0 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/schema v1.2.0
	github.com/klauspost/compress v1.10.10 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1 // indirect
	github.com/tkrajina/gpxgo v1.0.1
	go.mongodb.org/mongo-driver v1.3.5
	go.opencensus.io v0.22.4 // indirect
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899 // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208 // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	googlemaps.github.io/maps v1.2.1
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
