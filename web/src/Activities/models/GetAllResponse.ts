import { Activity } from ".";

export interface GetAllResponse {
  totalCount: number;
  results: Activity[];
};
