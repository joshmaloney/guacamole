export interface Bounds {
  minLat: number;
  minLng: number;
  maxLat: number;
  maxLng: number;
};
