
export interface ActivityTypeAggregation {
  // TODO(me): Convert to lower case once JSON is updated
  Name: string;
  Total: number;
};
