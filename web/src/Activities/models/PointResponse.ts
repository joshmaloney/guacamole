import { Point } from "./Point";

export interface PointResponse {
  id: string;
  points: Point[];
};
